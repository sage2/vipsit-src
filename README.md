Server for SAGE2 services:

* Process video files to web compatible MP4
* Convert large images to DZI
	* runs DZI conversion
		 * command: vips dzsave <filename> <output name> --layout dz --overlap 0 --tile-size 512 --suffix=.jpg\[Q=85\]
	* hosts the data for viewing


Dependencies:

   * exiftool
   	* for metadata extraction
   * ffmpeg
   	* for video conversion
   * libvips
   	* for image processing
	* https://libvips.github.io/libvips/
	* https://github.com/libvips/libvips
	* for sources, you will need
		* **libgsf-1**: libvips adds support for creating **image pyramids with dzsave**.
		* OpenSlide: If available, libvips can load OpenSlide-supported virtual slide files: Aperio, Hamamatsu, Leica, MIRAX, Sakura, Trestle, and Ventana
		* Also: libjpeg, libexif, giflib, librsvg, PDFium, libpoppler, libtiff, fftw3, lcms2, lcms
, libpng, libimagequant, ImageMagick, or optionally GraphicsMagick, pangoft2, orc-0.4, matio, cfitsio, libwebp, libniftiio, OpenEXR

