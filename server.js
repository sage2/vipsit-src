// OS level stuff
var fs           = require('fs');
var path         = require('path');
var ChildProcess = require('child_process');
var fse          = require('fs-extra')
var mv           = require('mv');
var path         = require('path');
var url          = require('url');

// express and http stuff
var https      = require('https');
var http       = require('http');
var express    = require('express');
var bodyParser = require('body-parser')
var multer     = require('multer');

// uploading management
var upload = multer({ dest: 'tmp/' })
var sanitize = require("sanitize-filename");

var app = express();
// to support URL-encoded bodies
app.use(bodyParser.urlencoded({ extended: false })); 

// Change for your local installtion
var serverPORT = 4000;
var serverURL  = "http://sage2rtt.evl.uic.edu:" + serverPORT + "/";


var myhttp = http.createServer(app).listen(serverPORT, function () {
	console.log('App listening for HTTP on port', serverPORT);
});
myhttp.timeout = 25 * 60 * 1000; // 25 min.

// Comment out HTTPS portion if only on HTTP needed
// Replace with your HTTPS certificate
// var privateKey  = fs.readFileSync('keys/_.evl.uic.edu.key', 'utf8');
// var certificate = fs.readFileSync('keys/_.evl.uic.edu.crt', 'utf8');
// var credentials = {key: privateKey, cert: certificate};
// var myhttps = https.createServer(credentials, app).listen(3043, function () {
	// console.log('App listening for HTTPS on port 3043!');
// });
// myhttps.timeout = 15 * 60 * 1000; // 15 min.

app.get('/upload', function (req, res) {
  res.redirect('/');
});
app.get('/video', function (req, res) {
  res.redirect('/');
});
app.get('/info', function (req, res) {
  res.redirect('/');
});


app.get('/imagelist', function (req, res) {
	var imagelist = [
	];

	fs.readdir( "public", function(err, list) {
		if (err) throw err;
		var regex = new RegExp(".dzi");
		list.forEach( function(item) {
			if( regex.test(item) ) {
				var basename = item.split('.')[0];

				var obj = {
					name: item,
					link: item,
					image: basename + "_files/9/0_0.jpg"
				};
				imagelist.push(obj);
			}
		}); 
		res.json(imagelist);
	});
});

app.use(express.static('public'));

app.post('/upload', upload.single('upload'), function (req, res, next) {

	var file = req.file;
	console.log("Got image upload> ", file.originalname);

	var newfilename = sanitize(file.originalname, {replacement: "_"}).replace(/ /g, "_");
	newfilename = newfilename.replace(/\&/g, "_");
	var destination = path.join('uploads', newfilename);

	mv(file.path, destination, function(err) {
		if (err) {
			console.log('Error> moving file', newfilename);
			res.end();
		} else {
			console.log('File> moved to', destination);

			exifFile(destination, function(error, metadata) {
				if (error) {
					console.log('Error> metadata processing');
					res.end();
				} else {
					console.log('EXIF>', metadata.FileName);
					console.log('EXIF>', metadata.FileType, metadata.MIMEType);
					console.log('EXIF>', metadata.ImageWidth, 'x', metadata.ImageHeight);

					var basename = metadata.FileName.split('.')[0];

					// cleanup
					var cleanup = path.join('public', basename + '.dzi');
					fse.removeSync(cleanup);
					cleanup = path.join('public', basename + '_files');
					fse.removeSync(cleanup);

					vipsFile(destination, basename, function(error) {
						if (error) {
							console.log('Error> VIPS processing', error);
							res.end();
						} else {
							var output = path.join('public', basename + ".dzi");
							console.log('VIPS> processing done', output);

							var dzi = dziFile(metadata.ImageWidth, metadata.ImageHeight, basename);
							fs.writeFileSync(output, dzi);
							res.redirect(302, basename + ".dzi");
						}
					});
				}
			});
		}
	});
});

app.post('/uploadvideo', upload.single('upload'), function (req, res, next) {

	var file = req.file;
	console.log("Got video upload> ", file.originalname);

	var newfilename = sanitize(file.originalname, {replacement: "_"}).replace(/ /g, "_");
	newfilename = newfilename.replace(/\&/g, "_");
	var destination = path.join('uploads', newfilename);

	mv(file.path, destination, function(err) {
		if (err) {
			console.log('Error> moving file', newfilename);
			res.end();
		} else {
			console.log('File> moved to', destination);

			exifFile(destination, function(error, metadata) {
				if (error) {
					console.log('Error> metadata processing');
					res.end();
				} else {
					console.log('EXIF>', metadata.FileName);
					console.log('EXIF>', metadata.FileType, metadata.MIMEType);
					console.log('EXIF>', metadata.ImageWidth, 'x', metadata.ImageHeight);

					var basename = metadata.FileName.split('.')[0];

					var quality = req.body.quality || '1080p';

					ffmpegFile(destination, basename, quality, function(error) {
						if (error) {
							console.log('Error> FFMPEG processing', error);
							res.end();
						} else {
							console.log('FFMPEG> processing done');

							res.redirect(302, basename + ".mp4");
						}
					});
				}
			});
		}
	});
});
 

// Functions
//

function dziFile(width, height, name) {
	var dzi;

	dzi  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	dzi += "<Image xmlns=\"http://schemas.microsoft.com/deepzoom/2008\"\n";
	dzi += "Format=\"jpg\"\n";
	dzi += "Overlap=\"0\"\n";
	dzi += "TileSize=\"512\"\n";
	dzi += "Url=\"" + serverURL + name + "_files/\"\n";
	dzi += ">\n";
	dzi += "<Size \n";
	dzi += "Height=\"" + height + "\"\n";
	dzi += "Width=\""  + width  + "\"\n";
	dzi += "/>\n";
	dzi += "</Image>\n";

	return dzi;
}

function ffmpegFile(filename, basename, quality, done) {
	// ffmpeg -i in-prores.mov -pix_fmt yuv420p -vcodec libx264 -preset medium -qp 18 -b:a 256K out.mp4

	var command;
	var output = path.join('public', basename + '.mp4');
	var args = "-pix_fmt yuv420p -vcodec libx264 -preset medium -qp 18 -b:a 256K";
	var scaling = "";
	var extras = "";

	if (quality === "720p") {
		extras = " -b:v 3M -maxrate 6M -bufsize:v 6M ";
		scaling = "-vf \"scale=min(iw\\,1280):min(720\\,ih)\"";
	} else if (quality === "1080p") {
		extras = " -b:v 6M -maxrate 8M -bufsize:v 12M ";
		scaling = "-vf \"scale=min(iw\\,1920):min(1080\\,ih)\"";
	} else if (quality === "2160p") {
		extras = " -b:v 15M -maxrate 30M -bufsize:v 15M ";
		scaling = "-vf \"scale=min(iw\\,3840):min(2160\\,ih)\"";
	}

	// var args2 = "-threads 8 " + scaling + " -vcodec nvenc -preset hp ";
	var args2 = "-threads 8 " + scaling + " -f mp4 -vcodec libx264 -preset fast ";
	args2 += extras;
	args2 += " -bf 2 -refs 1 -bf 2 -refs 1 -g 150 -i_qfactor 1.1 -b_qfactor 1.25 -qmin 1 -qmax 50";

	command = 'ffmpeg -i \"' + filename + '\" ' + args2 + ' -y ' + output;
	console.log('FFMPEG> command', command);
	ChildProcess.exec(command, function(error, stdout, stderr) {
		if (error !== null) {
			done(error);
		} else {
			console.log('FFMPEG done', output);
			done(null);
		}
	});
}

function vipsFile(filename, basename, done) {
	// vips dzsave filename ratbrain --layout dz --overlap 0 --tile-size 512 --suffix=.jpg\[Q=85\]           

	var command;
	var output = path.join('public', basename);
	command = 'vips dzsave \"' + filename + '\" ' + output + ' --layout dz --overlap 0 --tile-size 512 --suffix=.jpg';
	ChildProcess.exec(command, function(error, stdout, stderr) {
		if (error !== null) {
			done(error);
		} else {
			console.log('VIPS done', output);
			done(null);
		}
	});
}

function exifFile(filename, done) {
	ChildProcess.exec('exiftool -m -json -filesize# -all \"' + filename + '\"', function(error, stdout, stderr) {
		var metadata;
		if (error !== null) {
			if (stdout && stderr.length === 0) {
				// There's some output, it might just be an unknown file type
				metadata = JSON.parse(stdout);
				if ('SourceFile' in metadata[0]) {
					if (metadata[0].Error) {
						// if there was an error because unknown file type, delete it
						delete metadata[0].Error;
					}
					// Add a dummy type
					metadata[0].MIMEType = 'text/plain';
					metadata[0].FileType = 'text/plain';
					done(null, metadata[0]);
				} else {
					// unknown data
					done(error);
				}
			} else {
				// No output, it's really an error
				done(error);
			}
		} else {
			metadata = JSON.parse(stdout);
			done(null, metadata[0]);
		}
	});
}

